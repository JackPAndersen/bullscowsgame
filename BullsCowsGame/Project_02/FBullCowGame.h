/* The game logic ( no view code or direct user interaction)
The game is a simple guess the word game based on mastermind
*/


#pragma once
#include <string>

// to make the syntax unreal friendly
using FString = std::string;
using int32 = int;


// all values initialized to zero
struct FBullCowCount 
{
	
	int32 Bulls = 0;
	int32 Cows = 0;

};

enum class EGuessStatus {
	
	Invalid_Status,
	OK,
	Not_Isogram,
	Wrong_length,
	Not_Lowercase

};



class FBullCowGame 
{

public:
	FBullCowGame(); // constructor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;


	bool IsGameWon () const;
	EGuessStatus CheckGuessValidity(FString) const;

	void Reset(); 

	FBullCowCount SubmitValidGuess(FString);



// PLease try and ignore this and cous on the interface above
private:
	int32 MyCurrentTry; // see constructor for initialization
	FString MyHiddenWord;
	bool bGameisWon;
	bool IsIsogram(FString) const;
	bool IsLowerCase(FString) const;

};