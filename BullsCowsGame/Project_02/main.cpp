#include <iostream>
#include <string>
#include "FBullCowGame.h"
#pragma once

/* 
This is the console executable, that makes use of the BullCow class
This acts as the view in a MVC pattern, and is responsible for all
user interaction. For game logic see the FBullCowGame class.
*/

//to make syntax unreal friendly
using FText = std::string;
using int32 = int32;

//function prototypes as outside a class
void PrintIntro();
void PlayGame();
FText GetValidGuess();
bool AsktoPlayAgain();
void PrintGameSummary();

FBullCowGame BCGAME; // instantiate a new game�, which we re-use across plays

int main() 
{

	bool bPlayAgain = false;

	do 
	{
		PrintIntro();
		PlayGame();

		bPlayAgain = AsktoPlayAgain();
	} while (bPlayAgain);

	return 0;

}




void PrintIntro()
{
	
	std::cout << "\n\n Welcome to Bulls and Cows, a fun word game!" << std::endl;
	std::cout << "Can you guess the " << BCGAME.GetHiddenWordLength();
	std::cout << " letter isogram that I'm thinking of?" << std::endl;
	std::cout << std::endl;


}

//Plays a single game to completion

void PlayGame()
{

	BCGAME.Reset();
	int32 MaxTries = BCGAME.GetMaxTries();


	// loop asking for guesses while the game is NOT won
	// and there are still tries remaining
	while (!BCGAME.IsGameWon() && BCGAME.GetCurrentTry() <= MaxTries)
	{
		FText Guess = GetValidGuess();

		// Submit valid guess to the game, and receive counts
		FBullCowCount BullCowCount =  BCGAME.SubmitValidGuess(Guess);
		// Print number of bulls and cows
		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << " Cows = " << BullCowCount.Cows << "\n\n";
		
		std::cout << std::endl;
	}

	PrintGameSummary();
	


}


//get a guess from the player
FText GetValidGuess() // loop continually til you get a valid guess
{
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::Invalid_Status;

	do {
		int32 CurrentTry = BCGAME.GetCurrentTry();
		std::cout << "Try " << CurrentTry << " of " << BCGAME.GetMaxTries();
		std::cout <<  ". Enter your guess: ";
	
		std::getline(std::cin, Guess);

		 Status = BCGAME.CheckGuessValidity(Guess);

		switch (Status)
		{
		case EGuessStatus::Wrong_length:
			std::cout << "Please enter a " << BCGAME.GetHiddenWordLength() << " letter word. \n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Please enter a Isogram. \n\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please enter all lowercase letters. \n\n";
			break;


		default:
			// assume the guess is valid
			break;
		}
		
	} while (Status != EGuessStatus::OK); // keep looping until we get no errors
	return Guess;
}

bool AsktoPlayAgain()
{
	std::cout << "Do you want to play again with the same hidden word?(y/n)";
	FText Response = "";
	std::getline(std::cin, Response);
	
	return (Response[0] == 'y' || Response[0] == 'Y');

}

void PrintGameSummary() 
{
	if (BCGAME.IsGameWon())
	{
		std::cout << "WELL DONE - YOU WIN! \n";
	}

	else 
	{
		std::cout << " YOU LOSE - BETTER LUCK NEXT TIME!\n";
	}
}
