#include "FBullCowGame.h"
#include <map>
#define TMap std::map
#pragma once


// to make the syntax friendly

using FString = std::string;
using int32 = int;

FBullCowGame::FBullCowGame()   // default constructor
{
	Reset();
}



int32 FBullCowGame::GetCurrentTry() const { return  MyCurrentTry; }

int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }

bool FBullCowGame::IsGameWon() const { return bGameisWon; }

int32 FBullCowGame::GetMaxTries() const
{
	TMap< int32, int32 > WorldLengthToMaxTries{ { 3,4 }, { 4,7 }, { 5,10 }, {6,15}, {7,20} };
	return WorldLengthToMaxTries[MyHiddenWord.length()];

} 


void FBullCowGame::Reset() 
{
	const FString HIDDEN_WORD = "planet"; // this MUST be an isogram
	MyHiddenWord = HIDDEN_WORD;
	bGameisWon = false;
	MyCurrentTry = 1;
	
}


EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const 
{ 
	// if the guess isn't an isogram 
	if (!IsIsogram(Guess)) 
	{
		return EGuessStatus::Not_Isogram;
	}
	
	// if the guess isn't all lowercase 
	else if (!IsLowerCase(Guess))  
	{
		return EGuessStatus::Not_Lowercase;
	}
	
	// if the guess length is wrong
	else if (Guess.length() != GetHiddenWordLength())
	{
		return EGuessStatus::Wrong_length;
	}

	else 
	{
		return EGuessStatus::OK;
	}
} 

// receives a VALID guess, increments turn and returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{

	MyCurrentTry++;
	FBullCowCount BullCowCount;
	// loop through all letter in the Hidden Word
	int32 WordLength = MyHiddenWord.length(); // assume the same length as guess
	for (int32 i = 0; i < WordLength; i++)  
		// compare letters against the guess
	{
		for (int32 j = 0; j < WordLength; j++)
			// if the match then 
		{
			if (Guess[i] == MyHiddenWord[j])
				//  if they're in the same place
			{
				if (i == j) 
				{
					BullCowCount.Bulls++; 	// increment bulls
				}
				
				else 
				{ 
					BullCowCount.Cows++; // incriment cows
				}
			}
		}
		if (BullCowCount.Bulls == WordLength) 
		{
			bGameisWon = true;
		}

		else 
		{
			bGameisWon = false;
		}
	}
	return BullCowCount;
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	// treat 0 and 1 letter strings as isogram
	if (Word.length() <= 1) { return true; }

	
	TMap <char, bool> LetterSeen; //setup our map

	for (auto Letter : Word) // for all letters of the word
	{
		Letter = tolower(Letter); // handle mixed case
		
		if (LetterSeen[Letter]) // if the letter os om the map
		{
			return false; // we do NOT have an Isogram
		}
		
		else
		{
			LetterSeen[Letter] = true;  // add the letter to the map as seen
		}
			
			
	}	

	

	return true; // for example in cases where /0 is entered
}

bool FBullCowGame::IsLowerCase(FString Word) const
{

	for (auto Letter : Word) 
	{
		if (!islower(Letter)) 	// if not a lowercase letter 
		{
			return false; 
		}
		else 
		{
			return true;
		}
			
	}
	
	return false;
}





